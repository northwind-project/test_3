﻿
namespace NorthWindSolution.Core
{
    /// <summary>
    /// Base data service
    /// </summary>
    public class CoreDataService
    {
        #region Private variables
        /// <summary>
        /// Connection string to access database
        /// </summary>
        private readonly string conString = @"Data Source = LAPTOP-ICN8JHCP;initial catalog=Northwind;user id=sa;password=1qaz2wsx@";
        #endregion

        #region Public Constructor
        public CoreDataService()
        {
            this.ConnectionString = conString;
        }
        #endregion

        #region Properties
        public string ConnectionString { get; set; }    //Get or sets connection string
        #endregion
    }
}
